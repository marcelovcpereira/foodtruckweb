<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'email' =>  'marcelovcpereira@gmail.com',
            'name' => 'Marcelo Pereira',
            'password' => bcrypt('12345')
        ]);
       $user->save();
    }

}

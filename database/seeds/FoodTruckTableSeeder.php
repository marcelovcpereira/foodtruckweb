<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\FoodTruck;
use App\Location;
use App\Address;

class FoodTruckTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $foodtruck = FoodTruck::create([
            'name' => 'A Dogueria',
            'description' => 'Loja - Rua Booker Pittman, 230 - Ch.Sto. Antonio - (11) 2679-4366 e (11) 98106-8855 com Kátia
Truck - Consulte programação pelo email katiatess@gmail.com',
            'email' => 'katiatess@gmail.com',
            'facebookUrl' => 'https://www.facebook.com/adogueria'
        ]);
        $foodtruck->save();

        $foodtruck2 = FoodTruck::create([
            'name' => 'Aleatorium',
            'description' => 'Foodtruck',
            'email' => 'gabriel@aleatoriumfoodtruck.com',
            'url' => 'http://www.aleatoriumfoodtruck.com/',
            'facebookUrl' => 'https://www.facebook.com/aleatoriumfoodtruck'
        ]);
        $foodtruck2->save();

        $foodtruck3 = FoodTruck::create([
            'name' => 'Amarillo Volksbeer',
            'description' => 'Cervejas Artesanais e Importadas',
            'email' => 'amarillobeer@gmail.com',
            'url' => '',
            'facebookUrl' => 'https://www.facebook.com/amarillobeer'
        ]);
        $foodtruck3->save();

        $location = Location::create([
            'lat' =>-23.614846,
            'lng' => -46.657656
        ]);
        $location->save();
        $address = Address::create([
            'type' => 'Alameda',
            'route' => 'Uapixana',
            'postalCode' => '04085-030',
            'number' => '89',
            'neighborhood' => 'Indianópolis',
            'city' => 'São Paulo',
            'state' => 'São Paulo',
            'country' => 'Brasil',
            'location_id' => $location->id
        ]);
        $address->save();
        $foodtruck3->addresses()->attach($address->id);

        $location = Location::create([
            'lat' =>-23.609105,
            'lng' =>-46.649631
        ]);
        $location->save();
        $address = Address::create([
            'type' => 'Avenida',
            'route' => 'Piassanguaba',
            'number' => '100',
            'postalCode' => '04060-001',
            'neighborhood' => 'Planalto Paulista',
            'city' => 'São Paulo',
            'state' => 'São Paulo',
            'country' => 'Brasil',
            'location_id' => $location->id
        ]);
        $address->save();
        $foodtruck2->addresses()->attach($address->id);

        $location = Location::create([
            'lat' =>-23.568382,
            'lng' =>-46.657052
        ]);
        $location->save();

        $address = Address::create([
            'type' => 'Avenida',
            'route' => 'Moema',
            'postalCode' => '04077-020',
            'neighborhood' => 'Moema',
            'number' => '84',
            'city' => 'São Paulo',
            'state' => 'São Paulo',
            'country' => 'Brasil',
            'location_id' => $location->id
        ]);
        $address->save();
        $foodtruck->addresses()->attach($address->id);
    }

}

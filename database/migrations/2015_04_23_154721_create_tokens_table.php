<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tokens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('token', 32)->unique();
			$table->integer('user_id')->unsigned();
			$table->datetime('expires_at');
			$table->timestamps();
		});

		Schema::table('tokens', function($table) {
	       $table->foreign('user_id')->references('id')->on('users');
	   });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tokens');
	}

}

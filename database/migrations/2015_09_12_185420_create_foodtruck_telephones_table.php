<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodtruckTelephonesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('foodtruck_telephones', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('foodtruck_id')->unsigned();
            $table->foreign('foodtruck_id')->references('id')->on('foodtrucks');
            $table->integer('telephone_id')->unsigned();
            $table->foreign('telephone_id')->references('id')->on('telephones');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('foodtruck_telephones');
	}

}

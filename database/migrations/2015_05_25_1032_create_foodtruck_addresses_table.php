<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodtruckAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('foodtruck_addresses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('foodtruck_id')->unsigned();
            $table->foreign('foodtruck_id')->references('id')->on('foodtrucks');
			$table->integer('address_id')->unsigned();
            $table->foreign('address_id')->references('id')->on('addresses');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('foodtruck_addresses');
	}

}

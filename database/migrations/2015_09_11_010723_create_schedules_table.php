<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('schedules', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('from', 5);
            $table->string('to', 5);
            $table->datetime('date');
            $table->integer('foodtruck_id')->unsigned();
            $table->foreign('foodtruck_id')->references('id')->on('foodtrucks');
            $table->integer('address_id')->unsigned();
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('schedules');
	}

}

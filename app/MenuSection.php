<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuSection extends BaseModel {

	protected $table = "menu_sections";
    protected $hidden = ['created_at','updated_at'];

    /**
     * Relationship configuration for CRUD
     * @return array
     */
    public function relations()
    {
        return [
            'menu' => ['belongsTo', 'menu'],
            'menuitems' => ['hasMany', 'menuitem']
        ];
    }

    /**
     * Validation rules for the attributes.
     *
     * @return array
     */
    public function validationRules()
    {
        return [
            'name' => 'required|min:3'
        ];
    }

    /**
     * Foodtruck relationship
     * @return LaravelRelation
     */
    public function menu()
    {
    	return $this->belongsTo("App\\Menu", "menu_id");
    }

    public function menuitems()
    {
    	return $this->hasMany('App\MenuItem', "menusection_id");
    }

    /**
     * Defines that when loading eagerly, the foodtruck will be returned.
     * 
     * @return LaravelRelation
     */
    public static function eager()
    {
        return self::with("menuitems");
    }


}

<?php namespace App;
use App\Exceptions\NoLocationFoundException;
use Geocoder;

/**
 * Created by PhpStorm.
 * User: marcelopereira
 * Date: 4/19/15
 * Time: 9:59 PM
 */
class FoodTruck extends BaseModel {

    protected $table = "foodtrucks";
    protected $hidden = ['created_at','updated_at'];


    /**
     * Relationship configuration for CRUD
     * @return array
     */
    public function relations()
    {
        return [
            'addresses' => ['belongsToMany', 'address'],
            'menu' => ['hasOne', 'menu'],
            'comments' => ['hasMany', 'comment'],
            'rates' => ['hasMany', 'rate'],
            'schedules' => ['hasMany', 'schedule'],
            'telephones' => ['belongsToMany', 'telephone']
        ];
    }

    /**
     * Locations relationship
     * @return LaravelRelation
     */
    public function telephones()
    {
        return $this->belongsToMany("App\\Telephone", "foodtruck_telephones", "foodtruck_id", "telephone_id")->withTimestamps();
    }

    public function schedules()
    {
        return $this->hasMany('App\Schedule', 'foodtruck_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'foodtruck_id');
    }

    public function rates()
    {
        return $this->hasMany('App\Rate', 'foodtruck_id')->with('user');
    }

    /**
     * Validation rules for the attributes.
     *
     * @return array
     */
    public function validationRules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:foodtrucks,email,'.$this->id
        ];
    }

    /**
     * Locations relationship
     * @return LaravelRelation
     */
    public function addresses()
    {
    	return $this->belongsToMany("App\\Address", "foodtruck_addresses", "foodtruck_id", "address_id")->orderBy('created_at', "DESC")->withTimestamps();
    }

    /**
     * Every foodtruck has a Menu.
     * 
     * @return Menu
     */
    public function menu()
    {
        return $this->hasOne('App\Menu', "foodtruck_id");
    }

    /**
     * Defines that when loading eagerly, the locations will be returned.
     * 
     * @return LaravelRelation
     */
    public static function eager()
    {
        return self::with("addresses.location", "menu.menusections.menuitems", "schedules.address");
    }

    /**
     * Performs a check-in on the Foodtruck.
     * It receives a lat/lng pair, creates a location from it, finds its correspondent address and 
     * insert this new address to this Foodtruck instance.
     *  
     * @param  float $lat Latitude of location
     * @param  float $lng Longitude of location
     * @return void
     */
    public function checkin($lat, $lng)
    {
        $location = new Location();
        $location->lat = $lat;
        $location->lng = $lng;
        if ($location->isValid()) {
            $address = Geocoder::getAddressFromLocation($location);
            $address->save();
            //if ($this->getCurrentAddress())
            //$this->addresses()->detach($this->getCurrentAddress()->id);
            $this->addresses()->attach($address->id);
            $this->push();
        }
    }

    /**
     * Returns the most recent address of this foodtruck.
     * 
     * @return Address Current address instance.
     */
    public function getCurrentAddress()
    {
        return $this->addresses()->orderBy('created_at', 'desc')->first();
    }
}
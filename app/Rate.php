<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contracts\AfterJsonDecode;
use ApiAuth;

class Rate extends BaseModel implements AfterJsonDecode {

	protected $table = "rates";
	protected $hidden = ['created_at','updated_at', 'id'];
	protected $fillable = ['rate'];

	public function relations()
	{
		return [
			'foodtruck' => ['belongsTo', 'foodtruck'],
			'user' => ['belongsTo', 'user']
		];
	}

	public function validationRules()
    {
        return ['rate' => 'required|integer|min:0|max:10'];
    }

    public function foodtruck()
	{
		return $this->belongsTo('App\Foodtruck', 'foodtruck_id');
	}

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function afterJsonDecode()
	{
		$this->user_id = ApiAuth::getUser()->id;
	}

	public static function eager()
    {
    	return self::with("user", "foodtruck");
    }

}

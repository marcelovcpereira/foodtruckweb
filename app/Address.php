<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contracts\AfterJsonDecode;
use App\Location;
use Geocoder;
use App\Foodtruck;
use App\Exceptions\MultipleAddressException;
use App\Exceptions\NoLocationFoundException;

class Address extends BaseModel implements AfterJsonDecode {

	protected $table = "addresses";
	protected $hidden = ['created_at','updated_at', 'pivot', 'location_id'];
	protected $fillable = ['type', 'route', 'number', 'postalCode', 'neighborhood', 'city', 'country'];
    public $timestamps = true;

	public function location()
	{
		return $this->belongsTo("App\\Location");
	}

    public function foodtrucks()
    {
        return $this->belongsToMany('App\\Foodtruck', "foodtruck_addresses", "address_id", "foodtruck_id")->withTimestamps();
    }

	public function afterJsonDecode()
	{
		$locations = Geocoder::getLocationFromString($this->getLocationString());
		if (count($locations) > 1) {
            throw new MultipleAddressException(json_encode($locations));
        } else if (count($locations) < 1) {
            throw new NoLocationFoundException();
        }
        $locations[0]->save();
		$this->location()->associate($locations[0]);
		$this->push();
	}

	public function getLocationString()
	{
		return $this->type . " " . $this->route . " , " . $this->number .
        " - " . $this->neighborhood . " - " . $this->city . " - " . $this->state .
        " - " . $this->country;
	}

    public function distance(Address $address2, $miles = false)
    {
        return $this->location->distance($address2->location, $miles);
    }

    public function getNearby($distance)
    {
        $return = [];
        $locations = $this->location->getNearby($distance);
        foreach ($locations as $loc) {
            $return[] = $loc->address()->with("location")->get();
        }
        return $return;
    }

	public function validationRules()
    {
        return [
            'type' => 'required|max:20',
            'route' => 'required',
            'postalCode' => 'required',
            'neighborhood' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required'
        ];
    }

    public static function eager()
    {
    	return self::with("location");
    }
}

<?php namespace App\Exceptions;

class NoLocationFoundException extends \Exception {

    public function __construct($message = "No location found with given address")
    {
        parent::__construct($message);
    }

}
<?php namespace App\Exceptions;
/**
 * Created by PhpStorm.
 * User: marcelopereira
 * Date: 5/18/15
 * Time: 7:20 PM
 */


/**
 * Class InvalidJsonObjectException
 * Throwed when there is an error parsing a json to an object
 *
 * @package App\Exceptions
 */
class EmptyResultException extends \Exception {

    public function __construct($message = "Query didn't return any results.")
    {
        parent::__construct($message);
    }

}
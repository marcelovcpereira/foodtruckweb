<?php namespace App\Exceptions;

class LocationException extends \Exception {

    public function __construct($status)
    {
        $message = "An error occurred: $status";
        parent::__construct($message);
    }

}
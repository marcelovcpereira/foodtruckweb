<?php namespace App\Exceptions;

class InvalidTokenException extends ApiAuthException {

	const MSG = "You're not authorized to access this resource (invalid_token).";
	const CODE = 1001;

	public function __construct($message = self::MSG)
	{
		parent::__construct($message);
		$this->setStatusCode(self::CODE);
	}
	
}
<?php namespace App\Exceptions;
    /**
     * Created by PhpStorm.
     * User: marcelopereira
     * Date: 5/18/15
     * Time: 7:20 PM
     */


/**
 * Class LaravelRelationNotFoundException
 *
 * @package App\Exceptions
 */
class LaravelRelationNotFoundException extends \Exception {

    public function __construct($relation = "undefined")
    {
        parent::__construct("Laravel relation mapping $relation not found or invalid.");
    }

}
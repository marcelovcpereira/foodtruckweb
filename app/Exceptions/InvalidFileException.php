<?php namespace App\Exceptions;
    /**
     * Created by PhpStorm.
     * User: marcelopereira
     * Date: 5/18/15
     * Time: 7:20 PM
     */


/**
 * Class InvalidFileException
 * Throwed when there is an error in validating the File
 *
 * @package App\Exceptions
 */
class InvalidFileException extends \Exception {

    public function __construct($message = "Error while validating the posted file.")
    {
        parent::__construct($message);
    }

}
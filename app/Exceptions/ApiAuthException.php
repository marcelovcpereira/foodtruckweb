<?php namespace App\Exceptions;

class ApiAuthException extends \Exception {

	protected $statusCode = 1000;
	protected $type = 'App\Exceptions\ApiAuthException';

	public function __construct($message = null)
	{
		parent::__construct($message);
		$this->setType(get_called_class());
	}

	public function getStatusCode()
	{
		return $this->statusCode;
	}

	public function setStatusCode($code)
	{
		if ($code !== null && $code > 0) {
			$this->statusCode = $code;
		}
	}

	public function getType()
	{
		return $this->type;
	}

	public function setType($type = "")
	{
		if (is_string($type) && strlen($type) > 0) {
			$this->type = $type;
		}
	}
}
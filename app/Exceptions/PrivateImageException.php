<?php
/**
 * Created by PhpStorm.
 * User: marcelopereira
 * Date: 6/6/15
 * Time: 6:47 PM
 */

namespace App\Exceptions;


class PrivateImageException extends \Exception {
    public function __construct($name)
    {
        parent::__construct("Error: Accessing a private file: $name");
    }

}


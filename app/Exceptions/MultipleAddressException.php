<?php namespace App\Exceptions;

class MultipleAddressException extends \Exception {

    public function __construct($message = __CLASS__)
    {
        parent::__construct($message);
    }

}
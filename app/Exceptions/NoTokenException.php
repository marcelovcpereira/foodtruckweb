<?php namespace App\Exceptions;

class NoTokenException extends ApiAuthException {

	const MSG = "You need an access token to make api requests.";
	const CODE = 1002;

	public function __construct($message = self::MSG)
	{
		parent::__construct($message);
		$this->setStatusCode(self::CODE);
	}
	
}
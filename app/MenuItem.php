<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends BaseModel {

	protected $table = "menu_items";
    protected $hidden = ['created_at','updated_at'];

    /**
     * Relationship configuration for CRUD
     * @return array
     */
    public function relations()
    {
        return [
            'menusection'=>['belongsTo', 'menusection']
        ];
    }

    /**
     * Validation rules for the attributes.
     *
     * @return array
     */
    public function validationRules()
    {
        return [
            'name' => 'required|min:3',
            'description' => 'required|min:3'
        ];
    }

    /**
     * Foodtruck relationship
     * @return LaravelRelation
     */
    public function menusection()
    {
    	return $this->belongsTo("App\\MenuSection", "menusection_id");
    }
}

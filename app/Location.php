<?php namespace App;

use DB;

class Location extends BaseModel
{

    protected $table = "locations";
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Location validation rules.
     * 
     * @return array
     */
    public function validationRules()
    {
        return [
            'lat' => 'required|numeric',
            'lng' => 'required|numeric'
        ];
    }

    /**
     * Returns the distance, in Km, to another location.
     * 
     * @param  Location $location2 Target location.
     * @param  boolean  $miles     Flag to request result in miles.
     * @return float              Distance between this location and $location2
     */
    public function distance(Location $location2, $miles = false)
    {
        $lat1 = $this->lat;
        $lng1 = $this->lng;
        $lat2 = $location2->lat;
        $lng2 = $location2->lng;
        $pi80 = M_PI / 180;
        $lat1 *= $pi80;
        $lng1 *= $pi80;
        $lat2 *= $pi80;
        $lng2 *= $pi80;

        $r = 6372.797; // mean radius of Earth in km
        $dlat = $lat2 - $lat1;
        $dlng = $lng2 - $lng1;
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $km = $r * $c;

        return ($miles ? ($km * 0.621371192) : $km);
    }

    /**
     * Returns nearby locations to this considering the given max distance.
     * 
     * @param  int $distance Max distance of search.
     * @return array Locations near to this.
     */
    public function getNearby($distance)
    {
        $return = [];
        $lat = $this->lat;
        $lon = $this->lng;
        $select = $this->getHaversine($lat, $lon);
        $result = DB::table($this->table)
            ->select(DB::raw($select))
            ->having('distance', '<', $distance)->get();
        if ($result) {
            foreach ($result as $loc) {
                $location = new Location();
                $location->id = $loc->id;
                $location->lat = $loc->lat;
                $location->lng = $loc->lng;
                $return[] = $location;
            }
        }
        return $return;

    }

    /**
     * Different implementations of Haversine formula.
     * 
     * @param  integer $index Index of selected formula.
     * @return string Selected formula.
     */
    protected function getHaversine($lat, $lng, $index = 3)
    {
        switch ($index) {
            case 1: return "((ACOS(SIN($lat * PI() / 180) * SIN(lat * PI() / 180) + COS($lat * PI() / 180) * COS(lat * PI() / 180) * COS(($lng - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance, id, lat, lng";
            case 2: return "111.045* DEGREES(ACOS(COS(RADIANS($lat)) * COS(RADIANS(lat)) * COS(RADIANS($lng) - RADIANS(lng)) + SIN(RADIANS($lat)) * SIN(RADIANS(lat)))) AS distance, id, lat, lng";
            case 3: 
            default:
                return "( 6371 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) AS distance, id, lat, lng";
        }
    }

    /**
     * Every location has a correspondent Address instance.
     * 
     * @return Address 
     */
    public function address()
    {
        return $this->hasOne("App\\Address");
    }
}

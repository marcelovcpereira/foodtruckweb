<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
/**
 * Access Token class used to authenticate users in the API.
 * @author Marcelo Pereira <marcelovcpereira@gmail.com>
 */
class Token extends Model {

	/**
	 * Time to live of a token in minutes
	 */
	const TOKEN_TTL = 60;
	/**
	 * Size (in bytes) of the token hash value.
	 */
	const TOKEN_SIZE = 16;

	/**
	 * Default field name for the token hash
	 */
	const TOKEN_FIELD = "_token";

	/**
	 * Returns the user of this token.
	 * 
	 * @return App\User		User owner of the token.
	 */
	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	/**
	 * Checks if a Token is valid.
	 * 
	 * @param  string $token Token to be checked.
	 * @return bool 	True if token is valid. False otherwise.
	 */
	public static function validate($token)
	{
		$instance = Token::where('token','=',$token)
					->where('expires_at', '>', new \DateTime("now"))
					->first();
		if ($instance) {
			return true;
		}
		return false;
	}

	/**
	 * Generates a new Access Token for the App_Key given.
	 * It returns the active token for this user in case it's still valid.
	 * 
	 * @param  string $key App Key for the API
	 * @return string      New or current active Access Token.
	 */
	public static function generate($key)
	{
		$user = User::where('email', '=', $key)->first();

		$now = new \DateTime("now");

		$instance = Token::where('user_id', '=', $user->id)
					->where('expires_at', '>', $now)
					->first();
		if ($instance) {
			$tokenValue = $instance->token;
		} else {
			$token = new Token();
			$token->user_id = $user->id;
			$token->token = Token::generateRandomToken();
			$token->expires_at = $now->modify("+" . self::TOKEN_TTL . " minutes");
			$token->save();
			$tokenValue = $token->token;
		}
		return $tokenValue;
	}

	public function expire()
	{
		$this->expires_at = new \DateTime("now");
		$this->save();
	}

    public function refresh()
    {
        $now = new \DateTime("now");
        $this->expires_at = $now->modify("+" . self::TOKEN_TTL . " minutes");
        $this->save();
    }

	/**
	 * Generates a new random number to be used as a Token Hash
	 * 
	 * @return string Token hash.
	 */
	private static function generateRandomToken()
	{
		return bin2hex(openssl_random_pseudo_bytes(self::TOKEN_SIZE));
	}
}

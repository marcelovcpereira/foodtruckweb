<?php namespace App\Contracts;

/**
 * Interface AfterJsonDecode
 * Must implement method afterJsonDecode that is called
 * right after a new instance of the BaseModel object has been created.
 * Useful for processing fields that are dynamic and need input before been populated.
 * @example:
 *
 * A user has a date_of_birth and age fields.
 * When a new user is created, the date_of_birth is passed as argument, but the age must be calculated
 * based on date_of_birth. 
 *
 * The User model could then implement AfterJsonDecode to add the age field at runtime:
 * User@afterJsonDecode:
 * $this->age = $this->calculateAgeByDateOfBirth($this->date_of_birth);
 *
 * @package App\Contracts
 */
interface AfterJsonDecode {
     public function afterJsonDecode();
}
<?php namespace App\Contracts;

/**
 * Interface JsonDecodable
 * Must implement method jsonDecode that receives
 * a $key => $value Json Object and returns a valid
 * instance of the class.
 *
 * @package App\Contracts
 */
interface JsonDecodable {
     public function jsonDecode($json);
}
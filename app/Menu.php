<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends BaseModel {

	protected $table = "menus";
    protected $hidden = ['created_at','updated_at'];

    /**
     * Relationship configuration for CRUD
     * @return array
     */
    public function relations()
    {
        return [
            'menusections' => ['hasMany', 'menusection'],
            'foodtruck' => ['belongsTo', 'foodtruck']
        ];
    }

    public function foodtruck()
    {
    	return $this->belongsTo('App\Foodtruck', "foodtruck_id");
    }

    /**
     * Validation rules for the attributes.
     *
     * @return array
     */
    public function validationRules()
    {
        return [
            'name' => 'required|min:3'
        ];
    }

    /**
     * Foodtruck relationship
     * @return LaravelRelation
     */
    public function menusections()
    {
    	return $this->hasMany("App\\MenuSection", "menu_id");
    }

    /**
     * Defines that when loading eagerly, the foodtruck will be returned.
     * 
     * @return LaravelRelation
     */
    public static function eager()
    {
        return self::with("menusections.menuitems");
    }

}

<?php namespace App;

/**
 * Created by PhpStorm.
 * User: marcelopereira
 * Date: 9/10/15
 * Time: 9:46 PM
 * @example:
 * {
    "schedule": {
        "address_id":"1",
        "foodtruck_id":"1",
        "date":"2015-09-12",
        "from":"08:00",
        "to":"20:00"
    },
    "_token":"3f952e6dead911c95cc4c841dd4739b1"
    }
 */
use Illuminate\Database\Eloquent\Model;

class Schedule extends BaseModel {

    protected $table = "schedules";
    protected $hidden = ['created_at','updated_at'];

    /**
     * Relationship configuration for CRUD
     * @return array
     */
    public function relations()
    {
        return [
            'foodtruck' => ['belongsTo', 'foodtruck'],
            'address' => ['belongsTo', 'address']
        ];
    }

    public function foodtruck()
    {
        return $this->belongsTo('App\Foodtruck', "foodtruck_id");
    }

    public function address()
    {
        return $this->belongsTo('App\Address', "address_id");
    }

    /**
     * Defines that when loading eagerly, the locations will be returned.
     *
     * @return LaravelRelation
     */
    public static function eager()
    {
        return self::with("address");
    }

    /**
     * Validation rules for the attributes.
     *
     * @return array
     */
    public function validationRules()
    {
        return [
            'date' => 'required|date',
            'from' => 'required|date_format:H:i',
            'to' => 'required|date_format:H:i'
        ];
    }

}
<?php namespace App\Services;

use Illuminate\Http\Request;
use App\Exceptions\NoTokenException;
use App\Exceptions\InvalidTokenException;
use Auth;
use App\Token;
use App\User;
/**
 * Service class used to perform ApiAuthentication via Access Tokens
 * and Key/Secret pairs.
 */
class ApiAuth {
	/**
	 * @var Illuminate\Http\Request
	 */
	private static $request;
	/**
	 * @var string
	 */
	const TOKEN = '_token';
	/**
	 * @var string
	 */
	private static $generatedToken;

	/**
	 * Saves the request instance.
	 *
	 * @param Illuminate\Http\Request $r Laravel request.
	 */
	public static function setRequest(Request $r)
	{
		self::$request = $r;
	}

	/**
	 * Verifies if the request contains a Token.
	 *
	 * @return bool True if request has a token. False otherwise.
	 */
	public static function hasToken()
	{
		if (self::$request->has(self::TOKEN)) {
			return true;
		} else if (self::$request->isJson() && self::$request->json(self::TOKEN)) {
			return true;
		}
		return false;
	}

	/**
	 * Extracts the Token from the request.
	 *
	 * @return string Token or null.
	 */
	public static function getToken()
	{
		if (self::$request->has(self::TOKEN)) {
			return self::$request->input(self::TOKEN);
		} else if (self::$request->isJson() && self::$request->json(self::TOKEN)) {
			return self::$request->json(self::TOKEN);
		}
		return null;
	}

	/**
	 * Asks the Token model to validate the request token.
	 *
	 * @return bool True if token is valid. False otherwise.
	 */
	public static function validateToken()
	{
		$retorno = false;
		if (self::hasToken()) {
			$token = self::getToken();
			if (Token::validate($token)) {
				$retorno = true;
			}
		}
		return $retorno;
	}

	/**
	 * Validates a pair of api Key and Secret. In case it's valid,
	 * a Token is generated and saved to be returned to the user.
	 *
	 * @param  string $apiKey    Key of the app using the API
	 * @param  string $apiSecret Secret of the app using the API
	 * @return bool            True if it's a valid key/secret pair. False otherwise.
	 */
	public static function validateLogin($apiKey, $apiSecret)
	{
		if (Auth::validate(['email' => $apiKey, 'password' => $apiSecret])) {
			self::generateToken($apiKey);
			return true;
		}
		self::$generatedToken = null;
		return false;
	}

	/**
	 * Returns the generated access token after a successful login.
	 *
	 * @return string Token
	 */
	public static function getGeneratedToken()
	{
		return self::$generatedToken;
	}

	/**
	 * Asks the Token model to generate another instance and associate it with
	 * the current User of the app.
	 */
	private static function generateToken($key)
	{
		self::$generatedToken = Token::generate($key);
	}

	/**
	 * Checks the request for a valid Access Token.
	 * If the token is valid, the request proceeds.
	 * Otherwise it will throw an error to be captured by the middleware.
	 *
	 * @param  Request $request Http Request
	 * @return bool True if request has a valid token. Otherwise an exception will be thrown.
	 * @throws   InvalidTokenException
	 * @throws   NoTokenException
	 */
	public static function validateRequest(Request $request)
	{
		self::setRequest($request);
		if (self::hasToken()) {
			if (self::validateToken()) {
				return true;
			} else {
				throw new InvalidTokenException();
			}
		} else {
			throw new NoTokenException();
		}
	}

	/**
	 * Returns an instance of App\Token for the current request token.
	 *
	 * @return App\Token Current request token object
	 */
	private static function getTokenObj()
	{
		$token = self::getToken();
		$obj = Token::where('token', '=', $token)->first();
		return $obj;
	}

	/**
	 * Returns the current authenticated User instance.
	 *
	 * @return App\User Authenticated user.
	 */
	public static function getUser()
	{
		$token = self::getTokenObj();
		if (!$token) return null;

		return User::eager()->where('id', '=', $token->user_id)->first();
	}

    public static function refreshToken()
    {
        $token = self::getTokenObj();
        $token->refresh();
    }

	public static function logout()
	{
		$token = self::getTokenObj();
		$token->expire();
	}
}
<?php namespace App\Services;

/**
 * Created by PhpStorm.
 * User: marcelopereira
 * Date: 5/17/15
 * Time: 2:53 PM
 */
use App\Exceptions\NoLocationFoundException;
use App\Exceptions\LocationException;
use App\Location;
use App\Address;

/**
 * Responsible for handling locations processing. It converts an address string into a
 * Location point (latitude, longitude).
 * @author Marcelo Pereira <marcelovcpereira@gmail.com>
 */
class Geocoder
{

    const GEOCODE_URL = "http://maps.googleapis.com/maps/api/geocode/json?";
    const REVERSE_GEOCODE_URL = "http://maps.googleapis.com/maps/api/geocode/json?latlng=";

    /**
     * Returns a Location instance of an address string.
     *
     * @param  string $address Route of address.
     * @return App\Location
     * @throws  NoLocationFoundException
     * @throws  LocationException
     */
    public static function getLocationFromString($address)
    {
        $return = self::curlFetch(self::getAddressUrl($address));
        return self::parseResponse($return);
    }

    /**
     * Fetches a URL using cURL.
     * 
     * @param  string $url URL to fetch
     * @return string      Result
     */
    private static function curlFetch($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($curl);
        curl_close($curl);
        return $return;
    }

    /**
     * Builds and returns a URL to perform a request to google api.
     *
     * @param  string $address String to be searched.
     * @return string          URL to make a google api request.
     */
    protected static function getAddressUrl($address)
    {
        return self::GEOCODE_URL . "address=" . urlencode($address);
    }

    /**
     * Receives a response from google api and extracts the information
     * needed to build the Location point of the Address.
     *
     * @param  string $response Google Api response
     * @return App\Location
     * @throws  NoLocationFoundException
     * @throws  LocationException
     */
    protected static function parseResponse($response)
    {
        $response = json_decode($response, true);
        $status = $response['status'];
        if ($status == "OK") {
            return self::getLocationsFromResults($response['results']);
        } else if ($status == "ZERO_RESULTS") {
            throw new NoLocationFoundException;
        } else {
            throw new LocationException($status);
        }
        return null;
    }

    /**
     * Extracts an instance of Location for each Google result.
     *
     * @param  array $results Google api resultset
     * @return array          Array of found locations.
     */
    protected static function getLocationsFromResults($results)
    {
        $return = array();
        foreach ($results as $element) {
            if (in_array('street_address', $element['types'])) {
                $formatedAddress = $element['formatted_address'];
                $geometry = $element['geometry'];
                $latitude = $geometry['location']['lat'];
                $longitude = $geometry['location']['lng'];
                $isPartialMatch = isset($element['partial_match']) ? $element['partial_match'] : null;
                $placeId = $element['place_id'];
                $types = $element['types'];

                $new = new Location();
                $new->lat = $latitude;
                $new->lng = $longitude;
                $return[] = $new;
            }
        }
        return $return;
    }

    /**
     * Returns an Address instance based on a Location.
     * It fetches and parses the google maps api information about a location.
     * 
     * @param  Location $location Location to be searched.
     * @return Address Address instance representing the given location
     */
    public static function getAddressFromLocation(Location $location)
    {
        $result = self::curlFetch(self::REVERSE_GEOCODE_URL . $location->lat . "," . $location->lng);
        $result = json_decode($result, true);
        if ($result['status'] == "OK") {
            foreach ($result['results'] as $component) {
                $types = $component['types'];
                if (in_array("street_address", $types)) {
                    $location->save();
                    $addr = self::parseAddress($component['address_components']);
                    $addr->location()->associate($location);
                    $addr->save();
                    return $addr;
                }
            }
            throw new \Exception('Error trying to parse result');
        } else {
            throw new \Exception("Error trying to fetch Location");
        }
    }

    /**
     * Parses a google maps api response and creates an Address instance.
     * @param  array $components Results from maps request
     * @return Address             Created Address instance
     */
    private static function parseAddress($components)
    {
        $address = new Address();
        foreach ($components as $component) {
            if (in_array('street_number',$component['types'])) {
                $address->number = $component['long_name'];
            } else if (in_array('route',$component['types'])) {
                $parts = explode(' ', $component['long_name']);
                $address->type = $parts[0];
                array_shift($parts);
                $address->route = implode(' ', $parts);
            } else if (in_array('neighborhood',$component['types'])) {
                $address->neighborhood = $component['long_name'];
            } else if (in_array('administrative_area_level_2',$component['types'])) {
                $address->city = $component['long_name'];
            } else if (in_array('administrative_area_level_1',$component['types'])) {
                $address->state = $component['long_name'];
            } else if (in_array('postal_code',$component['types'])) {
                $address->postalCode = $component['long_name'];
            } else if (in_array('country',$component['types'])) {
                $address->country = $component['long_name'];
            }
        }
        return $address;
    }

}
<?php namespace App;

use Geocoder;
/**
 * @author Marcelo Pereira <marcelovcpereira@gmail.com>
 * Date: 4/21/15
 * Time: 1:12 PM
 */
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Schema\Builder as Schema;
use Illuminate\Http\Request;
use DB;
use App\Location;
use App\Contracts\JsonDecodable;
use App\Contracts\AfterJsonDecode;
use App\Exceptions\NotABaseModelInstanceException;
use App\Exceptions\ModelNotFoundException;
use App\Exceptions\InvalidJsonObjectException;
use App\Exceptions\LaravelRelationNotFoundException;
use App\Token;
/**
 * Represents a basic CRUDable Model.
 * 
 */
abstract class BaseModel extends Model {

    private $validator;


    /**
     * Creates an instance of a BaseModel from a json string and verifies it's validity
     *
     * @param $object Class name of the resource being loaded
     * @param $json
     * @param $father array Array of type [field_name, field_value] that is set when it's a
     * nested call, so we need to set it's parent foreign key before saving or it will throw a database
     * exception.
     * @return mixed
     * @throws  NotABaseModelInstanceException
     * @throws  ModelNotFoundException
     * @transactional
     */
    public static function createFromJson($object, $json, $father = null)
    {
        try {
            DB::beginTransaction();
            //Obtaining correct repository
            $instance = self::getObject($object);
            //If this is a child object, set it's father
            if ($father !== null && is_array($father) and count($father) == 2) {
                $fieldName = $father[0];
                $fieldValue = $father[1];
                $instance->$fieldName = $fieldValue;
            }
            //Decoding Json Object
            if ($instance instanceof JsonDecodable) {
                $instance = $instance->jsonDecode($json);
            } else {
                $instance = self::jsonDecode($instance, $json);
            }
            //Post processing
            if ($instance instanceof AfterJsonDecode) {
                $instance->afterJsonDecode();
            }
            //Validating new object
            if (!$instance->isValid()) {
                throw new InvalidJsonObjectException($instance->getValidationErrors());
            }
            DB::commit();
            return $instance;
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Decodes a JSON into an instance of a BaseModel subclass.
     * 
     * @param  BaseModel $instance instance to be populated.
     * @param  string    $json     JSON object
     * @return BaseModel              BaseModel instance
     */
    private static function jsonDecode(BaseModel $instance, $json)
    {
        foreach ($json as $key => $value) {
            $key = strtolower($key);
            //skip id property when loading from json
            if ($key === $instance->getKeyName()) continue;
            if ($key === Token::TOKEN_FIELD) continue;
            if ($key === 'password') $value = $instance->hashPassword($value);
            if ($instance->isRelation($key)) {
                if (!$instance->isValid()) {
                    $msg = get_class($instance) . " : " . $instance->getValidationErrors();
                    throw new InvalidJsonObjectException($msg);
                }
                $instance->save();
                $value = is_array($value) ? $value : [$value];
                $method = self::getLaravelMethod($instance, $key);
                $modelName = self::getRelationModelName($instance, $key);
                foreach ($value as $val) {
                    $father = [self::getInstanceForeignKey($instance), $instance->id];
                    $obj = self::createFromJson($modelName, $val, $father);
                    $string = json_encode($obj);
                    if (strtolower($method) == 'attach') {
                        $instance->$key()->$method($obj->id);
                    } elseif (strtolower($method) == 'savemany') {
                        $instance->$key()->$method([$obj]);
                    } else {
                        $instance->$key()->$method($obj);
                    }
                }
                continue;
            }
            $instance->$key = $value;
        }
        return $instance;
    }

    /**
     * Searches the request for a valid Json object to be retrieved.
     * 
     * @param  string  $objectClass Classname of the object (subclass of BaseModel)
     * @param  Request $request     HTTP Request (injected)
     * @return [type]               [description]
     */
    public static function getRequestJsonObject($objectClass, Request $request)
    {
        $object = $request->json("$objectClass");
        if (!$object) {
            $object = $request->input("$objectClass");
            //If the object was not found in json or input, normalize the request
            //to get StringCase mismatchings
            if (!$object) {
                $objectClassLower = strtolower($objectClass);
                $lowerInput = json_decode(strtolower(json_encode($request->all())), true);
                $object = $lowerInput["$objectClassLower"];
                if (!$object) {
                    throw new InvalidJsonObjectException();
                }
            }
        }
        return $object;
    }

    public static function addRelated(BaseModel $instance, $class, $json)
    {
        list($relation, $method, $singular) = $instance->getRelationToObject($class);
        $method = strtolower(self::translateLaravelMethod($method));
        $father = null;
        if ($method != 'attach' && $method != "belongstomany") {
            $father = [self::getInstanceForeignKey($instance),$instance->id];
        }
        $instance2 = BaseModel::createFromJson($class, $json, $father);
        if ($method == 'attach' || $method == 'belongstomany') {
            $instance2->save();
            $instance->$relation()->$method($instance2->id);
        } elseif ($method == 'savemany') {
            $instance->$relation()->$method([$instance2]);
        } else {
            $instance->$relation()->$method($instance2);
        }
        $instance->push();
        return $instance2;
    }

    public function getRelated($object)
    {
        if ($this->isRelation($object)) {
            return $this->$object()->get();
        }

        throw new LaravelRelationNotFoundException($object);
    }

    private static function getInstanceForeignKey(BaseModel $instance)
    {
        $clazz = new \ReflectionClass(get_class($instance));
        return strtolower($clazz->getShortName()) . "_" . $instance->getKeyName();
    }

    /**
     * Default hashing function for models password field.
     * @param $rawPass
     * @return string
     */
    public function hashPassword($rawPass)
    {
        return bcrypt($rawPass);
    }

    /**
     * Returns an instance of the given BaseModel subclass.
     * 
     * @param  string $object Classname of a BaseModel subclass.
     * @return BaseModel      An intance of a BaseModel subclass.
     * @throws  NotABaseModelInstanceException
     * @throws  ModelNotFoundException
     */
    public static function getObject($object)
    {
        $namespace = "App";
        $className = "$namespace\\$object";
        if (class_exists($className)) {
            $obj = new $className;
            if (is_subclass_of($obj, __CLASS__)) {
                return $obj;
            } else {
                throw new NotABaseModelInstanceException($obj . ":" . $object);
            }
        } else {
            throw new ModelNotFoundException($className);
        }
    }

    /**
     * Updates a resource by a json.
     *
     * @param $json Resource to update.
     * @throws InvalidJsonObjectException
     */
    public function updateFromJson($json)
    {
        foreach ($json as $key => $value) {
                $this->$key = $value;
        }
        if ($this->isValid()) {
            $this->save();
        } else {
            throw new InvalidJsonObjectException($this->getValidator()->errors());
        }
    }

    /**
     * Default eager behavior of the model.
     * Override this method to eager load the relations as desired.
     *
     * ex: class Post: eager() { return self::with("comments"); }
     * @return mixed
     */
    public static function eager()
    {
        return self::where("id","<>","null");
    }

    /**
     * Returns the correct laravel method to insert related objects.
     * 
     * @param  string $relType Relation type defined under "relations" method of BaseModel.
     * @return string Laravel method to insert a new related object.
     * @throws  LaravelRelationNotFoundException
     */
    public static function getLaravelMethod(BaseModel $instance, $key)
    {
        $relation = $instance->relations()[$key];
        $relType = $relation[0];
        return self::translateLaravelMethod($relType);
        throw new LaravelRelationNotFoundException($relType);
    }

    /**
     * Translates a laravel relation type to the correct method of 
     * inserting related models.
     * example: if relation is belongsToMany, so the correct method for insertion is attach
     * 
     * @param  string $relation Relation type
     * @return string           Method name
     */
    public static function translateLaravelMethod($relation)
    {
        switch(strtolower($relation)) {
            case 'belongstomany':
                return 'attach';
                break;
            case 'hasmany':
                return 'saveMany';
            case 'hasone':
                return 'save';
                break;
            case 'belongsto':
                return 'associate';
                break;
        }
    }

    /**
     * Gets the model name of a related BaseModel given another BaseModel instance and the key
     * representing the relation.
     *
     * ex:
     * $ret = getRelationModelName($post, 'comments');
     * In thie case, $ret is going to be Comment
     * @param  BaseModel $instance BaseModel instance
     * @param  string    $key      Relation name
     * @return string              Singular name of related class
     */
    private static function getRelationModelName(BaseModel $instance, $key)
    {
        $relation = $instance->relations()[$key];
        return $relation[1];
    }

    /**
     * Default relationship configuration for models.
     * 
     * @return array
     */
    public function relations()
    {
        return [];
    }

    /**
     * Verifies if a field name represents a relationship.
     * 
     * @param  string  $key Field name
     * @return boolean      True if the field is a related model. False otherwise.
     */
    public function isRelation($key)
    {
        return (bool) isset($this->relations()[strtolower($key)]);
    }

    /**
     * Returns the relation type between this and another BaseModel class.
     * @param  string $object BaseModel classname
     * @return array         Array representing the relation
     */
    public function getRelationToObject($object)
    {
        foreach ($this->relations() as $method => $relation) {
            if (strtolower($object) == strtolower($method)) return [$method, $relation[0], $relation[1]];
            if (strtolower($object) == strtolower($relation[1])) return [$method, $relation[0], $relation[1]];
        }
        throw new \Exception($object . " is not a related model of " . get_called_class());
    }

    /**
     * Uses the validationRules implementation to validate all the fields of the instance.
     *
     * @return bool True if the instance is valid. False otherwise.
     */
    public function isValid()
    {
        $this->validator = Validator::make($this->getAttributes(), $this->validationRules());
        return $this->validator->passes();
    }

    /**
     * Returns the BaseModel subclass validator instance.
     * 
     * @return Validator
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * Returns the errors found during validation process.
     * 
     * @return array Validation errors
     */
    public function getValidationErrors()
    {
        return $this->getValidator()->errors();
    }

    /**
     * Every BaseModel subclass must implement this method.
     *
     * ex: function validationRules() { return ['name' => 'required|min:20']; }
     * This will make sure every model created has a 20-char (at least) name
     * 
     * @return array Array of validations
     */
    abstract function validationRules();

}
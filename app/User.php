<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Foodtruck;

class User extends BaseModel implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token', 'pivot'];

	/**
     * Validation rules for the attributes.
     *
     * @return array
     */
	function validationRules()
	{
		return [
			'name' => 'required|min:5',
			'email' => 'required|email|unique:users',
			'password' => 'required|min:32'
		];
	}

	/**
	 * A user may have several foodtrucks
	 * @return [LaravelRelation]
	 */
	public function foodtrucks()
	{
		return $this->belongsToMany('App\\Foodtruck', 'user_foodtrucks', 'user_id', 'foodtruck_id')->withTimestamps();
	}

	/**
	 * Mapping of User relations
	 * @return array
	 */
	public function relations()
	{
		return [
			'foodtrucks' => ['belongsToMany', 'foodtruck']
		];
	}

	/**
	 * Eager loading of User instances.
	 * 
	 * @return [LaravelRelation]
	 */
	public static function eager()
	{
		return self::with("foodtrucks.schedules.address", "foodtrucks.addresses.location", "foodtrucks.menu.menusections.menuitems");
	}
}

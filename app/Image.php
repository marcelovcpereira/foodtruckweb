<?php namespace App;

use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Facades\ApiAuth;
use App\Exceptions\InvalidFileException;
use App\Exceptions\PrivateImageException;
use File;

class Image extends BaseModel
{

    const IMAGE_PATH = "/images/";
    const STORAGE_UNIT = "local";
    private $content;

    /**
     * Searches an image by the public name.
     *
     * @param $name
     * @return mixed
     */
    public static function getByPublicName($name)
    {
        $image = Image::where('filename', '=', $name)->first();
        if (!$image->isPublic()) {
            throw new PrivateImageException($name);
        }
        return $image;
    }

    public function isPublic()
    {
        return (bool)$this->public;
    }

    /**
     * Receives an UploadedFile and saves as Object.
     *
     * @param UploadedFile $file
     * @throws  InvalidFileException
     */
    public static function createFromFile(UploadedFile $file)
    {
        //Creating file...
        $user = ApiAuth::getUser();
        $filePath = self::IMAGE_PATH . $user->email . "/" . $file->getClientOriginalName();
        $fileHash = md5($filePath . microtime());
        Storage::disk(self::STORAGE_UNIT)->put($filePath, File::get($file));

        //Associating into database...
        $image = new Image();
        $image->mime = $file->getClientMimeType();
        $image->original_filename = $file->getClientOriginalName();
        $image->filename = $fileHash;
        $image->user()->associate($user);
        if ($image->isValid()) {
            $image->save();
        } else {
            throw new InvalidFileException($image->getValidationErrors());
        }
        return $image;
    }

    /**
     * Searches the image
     * @param $localName
     * @return mixed
     */
    public static function getLocalImage($localName)
    {
        $user = ApiAuth::getUser();
        //Checks if the user is the owner of the image
        $image = Image::where('original_filename', '=', $localName)
            ->where('user_id', '=', $user->id)->first();
        return $image;
    }

    /**
     * Validation rules for the attributes.
     *
     * @return array
     */
    public function validationRules()
    {
        return [
            'filename' => 'required|min:32',
            'mime' => 'required|in:image/png,jpeg,jpg,gif,png,image/jpg,image/jpeg,image/gif',
            'original_filename' => 'required',
            'user_id' => 'required'
        ];
    }

    /**
     * Reads the contents of the image on disk and returns it.
     *
     * @return mixed
     */
    public function getContent()
    {
        if ($this->content === null) {
            $email = $this->user()->first()->email;
            $imgPath = self::IMAGE_PATH . $email . "/" . $this->original_filename;
            $this->content = Storage::disk(self::STORAGE_UNIT)->get($imgPath);
        }
        return $this->content;
    }

    /**
     * User owner of the image.
     * 
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

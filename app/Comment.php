<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contracts\AfterJsonDecode;
use ApiAuth;

class Comment extends BaseModel implements AfterJsonDecode {

	protected $table = "comments";
	protected $hidden = ['created_at','updated_at'];
	protected $fillable = ['comment'];
	const MAX_COMMENT_SIZE = 255;

	public function relations()
	{
		return [
			'foodtruck' => ['belongsTo', 'foodtruck'],
			'user' => ['belongsTo', 'user']
		];
	}

	public function afterJsonDecode()
	{
		$this->user_id = ApiAuth::getUser()->id;
	}

	public function foodtruck()
	{
		return $this->belongsTo('App\Foodtruck', 'foodtruck_id');
	}

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function validationRules()
    {
        return ['comment' => 'required|max:255'];
    }

    public static function eager()
    {
    	return self::with("user", "foodtruck");
    }
}

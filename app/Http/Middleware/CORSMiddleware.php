<?php namespace App\Http\Middleware;

use Closure;
/**
 * Adds a header to the response to make Cross origin requests available.
 * 
 */
class CORSMiddleware {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		return $next($request)->header('Access-Control-Allow-Origin' , '*')
            ->header('Access-Control-Allow-Methods','POST,GET,PUT')
            ->header('Access-Control-Allow-Headers','origin, x-csrftoken, content-type, accept');
	}
}
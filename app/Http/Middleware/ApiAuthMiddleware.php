<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use Config;
use ApiAuth;
use App\Exceptions\ApiAuthException;

class ApiAuthMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		try {
			ApiAuth::validateRequest($request);
			return $next($request);
		} catch (ApiAuthException $e) {
			return response()->json([
				'code' => $e->getStatusCode(),
				'msg' => $e->getMessage(),
				'type' => $e->getType()
			], 403);
		}
	}

}

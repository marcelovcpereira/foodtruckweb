<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/
/*PRIVATE*/ Route::get('api/User/me', ['uses' => 'ApiAuthController@getMe', 'middleware' => 'auth.api']);
/*PRIVATE*/ Route::post('api/Foodtruck/{id}/checkinAt/{lat}/{lng}', ['uses' => 'FoodtruckController@checkin', 'middleware' => 'auth.api'])->where(['id'=>'[0-9]+']);
/*PRIVATE*/ Route::get('api/User/myImages', ['uses' => 'ImageController@getMyImages', 'middleware' => 'auth.api']);
/**
 * Image Upload and Download Routes
 */
/*PUBLIC*/  Route::get('api/images/public/{image}', ['uses' => 'ImageController@getPublic']);
/*PRIVATE*/ Route::get('api/images/{image}', ['uses' => 'ImageController@get', 'middleware' => 'auth.api']);
/*PRIVATE*/ Route::post('api/images/upload', ['uses' => 'ImageController@post', 'middleware' => 'auth.api']);

/**
 * Geolocalization Routes
 */

/*PUBLIC*/ Route::get('api/geo/address/{address}', ['uses' => 'GeocoderController@getLocation' /*, 'middleware' => 'auth.api'*/]);

/*PUBLIC*/ Route::get('api/geo/distance/{a}/{b}', ['uses' => 'FoodtruckController@distance']);

/*PUBLIC*/ Route::get('api/geo/nearbyTo/{lat}/{lng}/{distance?}', ['uses' => 'FoodtruckController@getFoodtrucksNearbyTo'/*, 'middleware' => 'auth.api'*/]);

/*PUBLIC*/ Route::get('api/geo/{lat}/{lng}', ['uses' => 'GeocoderController@getAddress']);
/**
 * RESTful API Routes
 */
Route::group(['prefix' => 'api'], function() {
    //List (PUBLIC)
    Route::get('/{object}', 'ResourceController@getAll');
    //List Related (PUBLIC)
    Route::get('/{object}/{id}/{object2}', 'ResourceController@getRelated')->where(['id'=>'[0-9]+']);
    //Read (PUBLIC)
    Route::get('/{object}/{id}', 'ResourceController@get')->where(['id'=>'[0-9]+']);
    //Create
    Route::post('/{object}', ['uses' => 'ResourceController@post', 'middleware' => 'auth.api']);
    //Create Related (PUBLIC)
    Route::post('/{object}/{id}/add/{object2}', ['uses' => 'ResourceController@addRelated'] /*,'middleware' => 'auth.api']*/)->where(['id'=>'[0-9]+']);
    //Update
    Route::put('/{object}', ['uses' => 'ResourceController@put', 'middleware' => 'auth.api']);
});

/**
 * Authentication Routes
 */
/*PUBLIC*/  Route::post('/apiauth/register', 'ApiAuthController@create');
/*PUBLIC*/  Route::post('/apiauth/login', 'ApiAuthController@login');
/*PRIVATE*/ Route::get('/apiauth/logout', ['middleware' => 'auth.api', 'uses' => 'ApiAuthController@logout']);
/*PRIVATE*/ Route::get('/apiauth/ping', ['uses' => 'ApiAuthController@ping', 'middleware' => 'auth.api']);
<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ApiAuth;
use App\User;
use Illuminate\Support\Facades\Redirect;
use App;

class ApiAuthController extends Controller {

	const API_KEY = 'api_key';
	const API_SECRET = 'api_secret';

	public function logout(Request $request)
	{
		ApiAuth::logout();
		return response()->json([
			'status' => 200,
			'msg' => 'You\'ve successfully logged out.'
		], 200);
	}

	public function login(Request $request)
	{
		if ($request->has(self::API_KEY) && $request->has(self::API_SECRET)) {
			$apiKey = $request->input(self::API_KEY);
			$apiSecret = $request->input(self::API_SECRET);
			if (ApiAuth::validateLogin($apiKey, $apiSecret)) {
				return response()->json([
					'status' => 200,
					'msg' => "You're successful logged in.",
					'_token' => ApiAuth::getGeneratedToken()
				]);
			} else {
				return response()->json([
					'status' => 401,
					'msg' => "Invalid Key/Secret pair"
				], 401);
			}
		}

		return response()->json([
			'status' => 401,
			'msg' => "You're trying to login without credentials."
		], 401);
	}

	public function create(Request $request)
	{
        return App::make('App\Http\Controllers\ResourceController')->post("user", $request);
//		try {
//			$user = User::create($request->all());
//			$user->save();
//
//			return response()->json([
//				'status' => 200,
//				'msg' => 'User created.',
//				'user' => json_encode($user)
//			]);
//		} catch (\Exception $e) {
//			return response()->json([
//				'status' => 500,
//				'msg' => $e->getMessage(),
//				'trace' => $e->getTrace()
//			]);
//		}
	}

    public function ping(Request $request)
    {
        if (ApiAuth::validateToken()) {
            ApiAuth::refreshToken();
            return response()->json([
                'status' => 200,
                'msg' => 'Valid token'
            ], 200);
        } else {
            return respomnse()->json([
                'status' => 401,
                'msg' => 'Invalid token'
            ], 401);
        }
    }

    public function getMe(Request $request)
    {
        return response()->json([
            'status' => 200,
            'User' => ApiAuth::getUser()
        ], 200);
    }
}
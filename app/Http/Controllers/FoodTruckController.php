<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Services\Geocoder;
use Illuminate\Foundation\Http\Response;
use Illuminate\Http\Request;
use App\Address;
use App\Location;
use App\Foodtruck;

class FoodTruckController extends Controller {

    public function distance($a, $b)
    {
        $a = Address::find($a);
        $b = Address::find($b);
        return response()->json([
            'distance' => $a->distance($b)
        ]);
    }

    public function getNearby($a, $distance)
    {
        $a = Address::find($a);
        return response()->json([
            'nearby' => $a->getNearby($distance),
            'test' => $a->foodtrucks()->get()
        ]);
    }

    public function getLocationsNearbyTo($lat, $lng, $distance)
    {
        $location = new Location();
        $location->lat = $lat;
        $location->lng = $lng;
        $locations = $location->getNearby($distance);
        return $locations;
    }

    public function getAddressesNearbyTo($lat, $lng, $distance)
    {
        $addresses = [];
        $locations = $this->getLocationsNearbyTo($lat, $lng, $distance);
        foreach ($locations as $loc) {
            $addresses[] = $loc->address;
        }
        return $addresses;
    }

    public function getFoodtrucksNearbyTo($lat, $lng, $distance = 20)
    {
        $return = [];
        $addresses = $this->getAddressesNearbyTo($lat, $lng, $distance);
        foreach ($addresses as $address) {
            foreach ($address->foodtrucks()->get() as $ft) {
                $ft->load('addresses.location');
                $return[] = $ft;
            }
        }
        if ($return && count($return)) {
            return response()->json([
                'status' => 302,
                'count' => count($return),
                'result' => json_encode($return)
            ]);
        } else {
            return response()->json([
                'status' => 401,
                'msg' => "No trucks for this position"
            ]);
        }
    }

    public function checkin($id, $lat, $lng)
    {
        try {
            $foodtruck = Foodtruck::findOrFail($id);
            $foodtruck->checkin($lat, $lng);
            $addr = $foodtruck->getCurrentAddress();
            return response()->json([
                'status' => 200,
                'msg' => 'Check-in performed.',
                'address' => $addr
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'msg' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
        }
    }

    public function teste()
    {
        $location = new Location();
        $location->lat = -23.608348;
        $location->lng = -46.641802;
        $result = Geocoder::getAddressFromLocation($location);
        return response()->json([
            'result' => $result
        ]);
    }
}

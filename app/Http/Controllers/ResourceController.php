<?php namespace App\Http\Controllers;

use App\BaseModel;
use App\Location;
use Illuminate\Foundation\Http\Response;
use Illuminate\Http\Request;
use App\Exceptions\EmptyResultException;

/**
 * Responsible for managing CRUD operations on BaseModel objects.
 * Any BaseModel subclass can be managed by this controller and will be available
 * to be queried via API.
 * 
 */
class ResourceController extends Controller
{
    /**
     * Retrieves a resource instance by id.
     * Every resource that extends BaseModel and sits at the App namespace
     * can be retrievable by this action.
     *
     * @param BaseModel $object Class name of the resource.
     * @param int $id Id of the resource to be retrieved.
     * @return JsonResponse
     */
    public function get($object, $id)
    {
        try {
            $repo = BaseModel::getObject($object);
            $instance = $repo::eager()->findOrFail($id);
            return response()->json([
                'status' => 200,
                "$object" => json_encode($instance->toArray())
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'errors' => $e->getMessage()
            ], 500);
        }
    }

    

    /**
     * Creates a new resource instance.
     *
     * @param BaseModel $object Class name of the resource.
     * @return JsonResponse
     */
    public function post($object, Request $request)
    {
        try {
            $json = BaseModel::getRequestJsonObject($object, $request);
            $instance = BaseModel::createFromJson($object, $json);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'msg' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 500);
        }

        if ($instance) {
            $instance->save();
            return response()->json([
                'status' => 201,
                'msg' => "$object created.",
                "$object" => $instance
            ], 201);
        } else {
            return response()->json([
                'status' => 400,
                'msg' => "Unexpected error. Report to server administration.",
            ], 400);
        }
    }

    /**
     * Adds a related model to another.
     * Ex:
     * addRelated("Post", 1, "Comment") will add a new Comment to the Post which id = 1
     * (the comment instance is in the payload of request)
     * 
     * @param string  $object  Receiver object
     * @param int  $id      Id od the receiver
     * @param string  $object2 Related object
     * @param Request $request HTTP request (injected)
     */
    public function addRelated($object, $id, $object2, Request $request)
    {
        try {
            $repo = BaseModel::getObject($object);
            $instance = $repo::eager()->findOrFail($id);
            $json =BaseModel::getRequestJsonObject($object2, $request);
            $newRelated = BaseModel::addRelated($instance, $object2, $json);
            return response()->json([
                'status' => 200,
                "$object" => json_encode($instance->toArray()),
                "related" => json_encode($newRelated->toArray())
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'errors' => $e->getMessage()/*,
                'trace' => $e->getTrace()*/
            ], 500);
        }
    }

    public function getRelated($object, $id, $object2)
    {
        try {
            $repo = BaseModel::getObject($object);
            $instance = $repo::findOrFail($id);
            $result = $instance->getRelated($object2);
            return response()->json([
                'status' => 200,
                'count' => count($result),
                'result' => $result
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'msg' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Retrieves a list of resource instances.
     *
     * @param BaseModel $object Class name of the resource.
     * @param Request $request HTTP request (injected)
     * @return JsonResponse
     * @throws EmptyResultException
     */
    public function getAll($object, Request $request)
    {
        //Query limit of results or default 5
        $quantity = $request->has('limit') ? $request->input('limit') : 5;
        //Query page or default 1
        $page = $request->has('page') ? $request->input('page') : 1;
        //Offset for db query
        $offset = ($page - 1) * $quantity;
        try {
            $repo = BaseModel::getObject($object);
            $result = $repo::eager()->whereNotNull('created_at')->skip($offset)->take($quantity)->get();

            if (count($result)) {
                return response()->json([
                    'status' => 302,
                    'count' => count($result),
                    'result' => json_encode($result)
                ], 302);
            } else {
                throw new EmptyResultException();
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'errors' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Updates an object instance.
     * 
     * @param  string  $object  Json object
     * @param  Request $request HTTP Request (injected)
     * @return Response
     */
    public function put($object, Request $request)
    {
        $json = $request->input("$object");
        if (isset($json['id'])) {
            $id = $json['id'];
            try {
                $repo = BaseModel::getObject($object);
                $instance = $repo::find($id);
                if ($instance) {
                    $instance->updateFromJson($json);
                    return response()->json([
                        'status' => 200,
                        'msg' => "Resource updated.",
                        "$object" => json_encode($instance)
                    ]);
                } else {
                    return response()->json([
                        'status' => 404,
                        'msg' => 'Resource not found with id $id'
                    ], 404);
                }
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 500,
                    'errors' => $e->getMessage()
                ], 500);
            }
        } else {

            return response()->json([
                'status' => 400,
                'msg' => 'No id specified.'
            ], 400);
        }
    }
}
<?php namespace App\Http\Controllers;

use App\Exceptions\InvalidFileException;
use App\Exceptions\PrivateImageException;
use File;
use Storage;
use Illuminate\Foundation\Http\Response;
use Illuminate\Http\Request;
use App\Image;
use ApiAuth;

class ImageController extends Controller {

    public function getMyImages(Request $request) {
        $user = ApiAuth::getUser();
        $images = Image::where('user_id', '=', $user->id)->get();
        if ($images) {
            return response()->json([
                'status' => 200,
                'images' => $images
            ], 200);
        } else {
            return response()->json([
                'status' => 500,
                'msg' => 'No image found'
            ]);
        }
    }

	/**
	 * Saves a new Image.
	 * 
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
    public function post(Request $request) {
    	if ($request->hasFile('image')) {
    		if ($request->file('image')->isValid()) {
                try {
                    $image = Image::createFromFile($request->file('image'));
                } catch (InvalidFileException $e) {
                    return response()->json([
                        'status' => 500,
                        'msg' => $e->getMessage()
                    ]);
                }
			} else {
				return response()->json([
	            	'status' => 500,
	            	'msg' => 'Invalid file'
        		]);
			}
    	} else {
    		return response()->json([
            	'status' => 500,
            	'msg' => 'File not found'
        	]);
    	}

        if ($request->has('redirUrl')) {
            return redirect('http://localhost:8080/perfil');
        }
        return response()->json([
            'status' => 200,
            'msg' => 'Image uploaded',
            'image' => json_encode($image)
        ]);
    }

    /**
     * Retrieves an Image.
     * @param  Request $request HTTP Request
     * @param  string  $image   Image name
     * @return Response         Image response or Error response
     */
    public function get(Request $request, $name)
    {
        $image = Image::getLocalImage($name);
		if ($image) {
			return response($image->getContent())->header('Content-Type', $image->mime);
		} else {
			return response()->json([
				'status' => 500,
				'msg' => "Image not found."
			]);
		}
    }

    /**
     * Retrieves an Image.
     * @param  Request $request HTTP Request
     * @param  string  $publicImageName   Image public name
     * @return Response         Image response or Error response
     */
    public function getPublic(Request $request, $publicImageName)
    {
        try {
            $image = Image::getByPublicName($publicImageName);
            if ($image) {
                return response($image->getContent())->header('Content-Type', $image->mime);
            } else {
                return response()->json([
                    'status' => 500,
                    'msg' => "Image not found."
                ]);
            }
        } catch (PrivateImageException $e) {
            return response()->json([
                'status' => 403,
                'msg' => $e->getMessage()
            ], 403);
        }
    }

}

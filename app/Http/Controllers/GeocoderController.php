<?php namespace App\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: marcelopereira
 * Date: 5/17/15
 * Time: 3:04 PM
 *
 */
use Illuminate\Foundation\Http\Response;
use Illuminate\Http\Request;
use App\Location;
use Geocoder;


class GeocoderController extends Controller {

    /**
     * Returns a Location instance based on an Address String.
     * 
     * @param Request $request HTTP request.
     * @param $address string String of the address to be translated to Location
     * @return Location[]
     */
    public function getLocation(Request $request, $address)
    {
        try {
            $location = Geocoder::getLocationFromString($address);
            return response()->json([
                'status' => 200,
                'msg' => 'Success',
                'location' => json_encode($location)
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function getAddress(Request $request, $lat, $lng)
    {
        try {
            $location = new Location();
            $location->lat = $lat;
            $location->lng = $lng;
            $address = Geocoder::getAddressFromLocation($location);
            return response()->json([
                'status' => 200,
                'msg' => 'Success',
                'address' => json_encode($address)
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'msg' => $e->getMessage()
            ]);
        }
    }
}
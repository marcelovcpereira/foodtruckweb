<?php
/**
 * Created by PhpStorm.
 * User: marcelopereira
 * Date: 9/12/15
 * Time: 3:38 PM
 */

namespace App;

/**
 * Class that saves numbers and code of user & foodtrucks phones.
 *
 * @author Marcelo Pereira <marcelovcpereira@gmail.com>
 */
class Telephone extends BaseModel{

    protected $table = "telephones";
    protected $hidden = ['created_at','updated_at'];


    /**
     * Validation rules for the attributes.
     *
     * @return array
     */
    public function validationRules()
    {
        return [
            'prefix' => 'required|min:2',
            'number' => 'required|min:8'
        ];
    }

}
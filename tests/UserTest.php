<?php

class UserTest extends TestCase {

	public function testLogin()
	{
		//bad login call
		$response = $this->call('POST', '/apiauth/login', []);
		$this->assertResponseStatus(401);

		//good login call
		$response = $this->call('POST', '/apiauth/login', [
			'api_key' => 'marcelovcpereira@gmail.com',
			'api_secret' => '12345'
		]);
		$this->assertArrayHasKey('_token', (array)$response->getData());
		$token = $response->getData()->_token;
		$this->assertEquals(200, $response->getStatusCode());
		//repeat call and check same token
		$response = $this->call('POST', '/apiauth/login', [
			'api_key' => 'marcelovcpereira@gmail.com',
			'api_secret' => '12345'
		]);
		$this->assertEquals($token, $response->getData()->_token);
	}

	public function testCreate()
	{
		
	}

}
